FROM python:3.10-slim

RUN apt-get update
RUN apt-get install git -y

COPY requirements.txt .

RUN pip install -r requirements.txt

COPY . /app

WORKDIR app

ENTRYPOINT ["python", "bouncing_ball_job.py"]