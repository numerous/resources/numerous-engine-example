import logging
from numerous.engine_tools.job import NumerousEngineJob
from numerous.image_tools.app import run_job

if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    run_job(numerous_job=NumerousEngineJob(), appname='bouncing-ball', model_folder='.')