from numerous.engine.system import Subsystem
from numerous.multiphysics import EquationBase, Equation
from numerous.engine.model import LoggerLevel

class BouncingBall(Subsystem, EquationBase):
    def __init__(self, tag='bouncing', g=9.81, f_loss=0.05, x0=1, v0=0):
        super().__init__(tag=tag)
        self.t1 = self.create_namespace('t1')
        self.add_parameter('g', g)
        self.add_constant('f_loss', f_loss)
        self.add_parameter('t_hit', 0, logger_level=LoggerLevel.INFO)
        self.add_state('x', x0, logger_level=LoggerLevel.INFO)
        self.add_state('v', v0, logger_level=LoggerLevel.INFO)
        self.t1.add_equations([self])

        def hitground_event_fun(t, states):
            return states['t1.x']

        def hitground_event_callback_fun(t, variables):
            velocity = variables['t1.v']
            velocity = -velocity * (1 - variables['t1.f_loss'])
            variables['t1.v'] = velocity
            variables['t1.t_hit'] = t

        self.add_event("hitground_event", hitground_event_fun, hitground_event_callback_fun)

    @Equation()
    def eval(self, scope):
        scope.x_dot = scope.v  # Position
        scope.v_dot = -scope.g  # Velocity


def entrypoint(tag, system):
    ball_component = system.components[tag]
    return BouncingBall(tag=tag, g=ball_component.inputs['t1.g'], **ball_component.constants)